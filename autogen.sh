#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

(test -f $srcdir/configure.in \
  && test -f $srcdir/README \
  && test -d $srcdir/src) || {
    echo -n "**Error**: Directory $srcdir does not look like the"
    echo " top-level pygtkimageview directory"
    exit 1
}

which gnome-autogen.sh || { 
    echo "You need to install gnome-common available from GNOME svn." 
    exit 1 
}

REQUIRED_AUTOMAKE=1.9
USE_GNOME_MACROS=1 NOCONFIGURE=1 . gnome-autogen.sh
echo $srcdir/configure "$@"
$srcdir/configure "$@"
