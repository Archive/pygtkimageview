######################################################################
##### Setup stuff ####################################################
######################################################################
AC_PREREQ(2.53)

AC_INIT(pygtkimageview, 1.2.0, [http://trac.bjourne.webfactional.com])

# foreign makes it so I don't need stupid NEWS, INSTALL, etc files.
AM_INIT_AUTOMAKE([foreign])

######################################################################
##### Check for different programs ###################################
######################################################################
AC_DISABLE_STATIC
AC_PROG_LIBTOOL

PKG_CHECK_MODULES(PYGTK, pygtk-2.0 >= 2.8.0)
AC_SUBST(PYGTK_CFLAGS)
AC_SUBST(PYGTK_LIBS)

PKG_CHECK_MODULES(PYCAIRO, pycairo)
AC_SUBST(PYCAIRO_CFLAGS)
AC_SUBST(PYCAIRO_LIBS)

PKG_CHECK_MODULES(GTKIMAGEVIEW, gtkimageview >= 1.5.0)
AC_SUBST(GTKIMAGEVIEW_CFLAGS)
AC_SUBST(GTKIMAGEVIEW_LIBS)

# Check for Python
AM_PATH_PYTHON(2.2)
AM_CHECK_PYTHON_HEADERS(,[AC_MSG_ERROR(could not find Python headers or library)])

AC_PATH_PROG(PYGOBJECT_CODEGEN, pygobject-codegen-2.0, no)
if test "x$PYGOBJECT_CODEGEN" = xno; then
  dnl This is for compat with older releases when codegen was shipped
  dnl in pygtk. It should be removed in future releases.
  AC_PATH_PROG(PYGOBJECT_CODEGEN, pygtk-codegen-2.0, no)
  if test "x$PYGOBJECT_CODEGEN" = xno; then
    AC_MSG_ERROR(could not find pygobject-codegen-2.0 script)
  fi
fi

AC_PATH_PROG(GLIB_MKENUMS, glib-mkenums, no)
if test "x$GLIB_MKENUMS" = xno; then
  AC_MSG_ERROR(could not find glib-mkenums program)
fi

AC_MSG_CHECKING(for pygtk defs)
PYGTK_DEFSDIR=`$PKG_CONFIG --variable=defsdir pygtk-2.0`
AC_SUBST(PYGTK_DEFSDIR)
AC_MSG_RESULT($PYGTK_DEFSDIR)

######################################################################
##### Twiddle CFLAGS #################################################
######################################################################
CFLAGS="${CFLAGS} -Wall -std=c99 -Wno-missing-prototypes"

######################################################################
##### Output files ###################################################
######################################################################
AC_CONFIG_FILES([
pygtkimageview.pc
Makefile
docs/Makefile
src/Makefile
tests/Makefile
])

AC_OUTPUT

echo "

Configuration:

        == Global settings ==
        Compiler:           ${CC}
        CFLAGS:             ${CFLAGS}

        == PyGTK settings ==
        PYGTK_CFLAGS:       ${PYGTK_CFLAGS}
        PYGTK_DEFSDIR:      ${PYGTK_DEFSDIR}
        PYGOBJECT_CODEGEN:  ${PYGOBJECT_CODEGEN}

        == Python settings ==
        pythondir:          ${pythondir}
        pkgpythondir:       ${pkgpythondir}

        == glib settings ==
        GLIB_MKENUMS:       ${GLIB_MKENUMS}

        == GtkImageView settings ==
        GTKIMAGEVIEW_CFLAGS ${GTKIMAGEVIEW_CFLAGS}
"
