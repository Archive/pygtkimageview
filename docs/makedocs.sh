EPYOPTS="--no-private -v --debug --config epydoc.config"

rm -rf html pdf
PYTHONPATH=. epydoc ${EPYOPTS} --html gtkimageview

# If you don't want to generate a PDF, comment out the following lines.
PYTHONPATH=. epydoc ${EPYOPTS} --pdf gtkimageview
mv pdf/api.pdf pdf/pygtkimageview-1.2.0.pdf
