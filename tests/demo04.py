'''
Shows how to implement an IImageTool.
'''
import gobject
import gtk
import gtk.gdk
import gtkimageview
import sys

class MyTool(gobject.GObject, gtkimageview.IImageTool):
    def __init__(self):
        gobject.GObject.__init__(self)
        self.crosshair = gtk.gdk.Cursor(gtk.gdk.CROSSHAIR)
        self.cache = gtkimageview.PixbufDrawCache()

    def do_button_press(self, ev_button):
        pass

    def do_button_release(self, ev_button):
        pass

    def do_motion_notify(self, ev_motion):
        pass

    def do_pixbuf_changed(self, reset_fit, rect):
        print 'pixbuf changed'

    def do_paint_image(self, draw_opts, drawable):
        self.cache.draw(draw_opts, drawable)

    def do_cursor_at_point(self, x, y):
        return self.crosshair
gobject.type_register(MyTool)

try:
    pixbuf = gtk.gdk.pixbuf_new_from_file(sys.argv[1])
except IndexError:
    print 'Usage: %s image' % sys.argv[0]
    sys.exit(1)

view = gtkimageview.ImageView()
view.set_pixbuf(pixbuf)

tool = MyTool()
view.set_tool(tool)

win = gtk.Window()
win.connect('delete-event', gtk.main_quit)
win.add(gtkimageview.ImageScrollWin(view))
win.set_default_size(500, 300)
win.show_all()
gtk.main();
