'''
Shows how to load SVG files in gtkimageview. It is really easy.
'''
import gtk
from gtk import gdk
import gtkimageview
import sys

try:
    import rsvg
except ImportError:
    print 'Sorry! rsvg is needed to run this demo.'
    sys.exit(1)

try:
    handle = rsvg.Handle(sys.argv[1])
except IndexError:
    print 'Usage: %s SVG-image' % sys.argv[0]
    sys.exit(1)
    
pixbuf = handle.get_pixbuf()
    
# Setup the view
view = gtkimageview.ImageView()
view.set_pixbuf(pixbuf)

win = gtk.Window()
win.connect('delete-event', gtk.main_quit)
win.add(gtkimageview.ImageScrollWin(view))
win.set_default_size(400, 300)
win.show_all()

gtk.main()
