'''
This program displays a pixbuf with a diagonal pattern. It can be used
to detect off by one errors in gtkimageview by scrolling the view. See
ticket #31.
'''
import gtk
from gtk import gdk
import gtkimageview

# Render a diagonal pattern.
data = ['\x00'] * 3 * 1600 * 1600
for n in range(0, 1600 * 1600, 3):
    data[n * 3] = '\xff'
    data[n * 3 + 1] = '\xff'
    data[n * 3 + 2] = '\xff'
for y in range(1600):
    for x in range(450):
        line_ofs = y * 1600 * 3
        data[line_ofs + x * 3] = '\xff'
        data[line_ofs + (1599 - x) * 3 + 1] = '\xff'
for y in range(450):
    for x in range(1600):
        data[y * 1600 * 3 + x * 3 + 2] = '\xff'

pixbuf = gtk.gdk.pixbuf_new_from_data(''.join(data),
                                      gtk.gdk.COLORSPACE_RGB, False, 8,
                                      1600, 1600,
                                      1600 * 3)
        
view = gtkimageview.ImageView()
view.set_interpolation(gdk.INTERP_NEAREST)
view.set_pixbuf(pixbuf)

scroll = gtkimageview.ImageScrollWin(view)

win = gtk.Window(gtk.WINDOW_TOPLEVEL)
win.set_default_size(700, 700)
win.connect('destroy', gtk.main_quit)
win.add(scroll)
win.show_all()
gtk.main()
