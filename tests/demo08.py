'''
Demonstrates the ImageToolCairo class. It is not yet part of
gtkimageview.
'''
import cairo
import cairotool
import gtk
from gtk import gdk
import gtkimageview
import sys

class CairoAppearance:
    def __init__(self):
        self.line_width = 5
        self.source_rgba = 0, 0, 1, 0.4
        self.operator = cairo.OPERATOR_OVER
        self.line_cap = cairo.LINE_CAP_ROUND
        self.line_join = cairo.LINE_JOIN_ROUND

    def setup_context(self, cr):
        cr.set_line_width(self.line_width)
        cr.set_source_rgba(*self.source_rgba)
        cr.set_operator(self.operator)
        cr.set_line_cap(self.line_cap)
        cr.set_line_join(self.line_join)

class RectShaper(cairotool.ImageCairoShaper):
    def __init__(self):
        self.base = None
        self.ofs = None
        self.app = CairoAppearance()
        self.app.line_width = 8
        self.app.source_rgba = 0, 1, 0, 0.2

    def start(self, x, y):
        self.base = self.ofs = x, y

    def reset(self):
        self.base = self.ofs = None

    def motion(self, x, y):
        self.ofs = x, y

    def get_damage_area(self):
        box = cairotool.gdk_rectangle_get_extents(self.base, self.ofs)
        return cairotool.gdk_rectangle_pad(box, self.app.line_width * 2)

    def draw(self, cr):
        if not self.ofs or not self.base:
            return
        self.app.setup_context(cr)
        cr.move_to(*self.base)
        box = cairotool.gdk_rectangle_get_extents(self.base, self.ofs)
        cr.rectangle(*box)
        cr.stroke()

class LineShaper(cairotool.ImageCairoShaper):
    def __init__(self):
        self.base = None
        self.ofs = None
        self.app = CairoAppearance()

    def start(self, x, y):
        self.base = self.ofs = x, y

    def reset(self):
        self.base = self.ofs = None

    def get_damage_area(self):
        box = cairotool.gdk_rectangle_get_extents(self.base, self.ofs)
        return cairotool.gdk_rectangle_pad(box, self.app.line_width * 2)

    def motion(self, x, y):
        self.ofs = x, y

    def draw(self, cr):
        if not self.base or not self.ofs:
            return
        self.app.setup_context(cr)
        cr.move_to(*self.base)
        cr.line_to(*self.ofs)
        cr.stroke()

class PenShaper(cairotool.ImageCairoShaper):
    def __init__(self):
        self.app = CairoAppearance()
        self.app.source_rgba = 1, 0, 0, 0.6
        self.points = []

    def start(self, x, y):
        self.points = [(x, y)]

    def reset(self):
        self.points = []

    def get_damage_area(self):
        box = cairotool.gdk_rectangle_get_extents(*self.points)
        return cairotool.gdk_rectangle_pad(box, self.app.line_width * 2)

    def motion(self, x, y):
        self.points.append((x, y))

    def draw(self, cr):
        if not self.points or len(self.points) == 1:
            return
        self.app.setup_context(cr)
        cr.move_to(*self.points[0])
        for point in self.points[1:]:
            cr.line_to(*point)
        cr.stroke()

try:
    pixbuf = gdk.pixbuf_new_from_file(sys.argv[1])
except IndexError:
    pixbuf = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB, False, 8, 800, 600)    

view = gtkimageview.ImageView()
view.set_interpolation(gdk.INTERP_NEAREST)

cairo_shapers = [PenShaper(),
                 LineShaper(),
                 RectShaper()]
tool = cairotool.ImageToolCairo(view, cairo_shapers[0])

view.set_tool(tool)
view.set_pixbuf(pixbuf)

def tool_changed_cb(action, current):
    value = current.get_current_value()
    if not value:
        view.set_tool(gtkimageview.ImageToolDragger(view))
    else:
        cairo_shaper = cairo_shapers[value - 1]
        tool.set_cairo_shaper(cairo_shaper)
        view.set_tool(tool)

tool_group = gtk.ActionGroup('tool')
tool_actions = [
            ('Select',
             gtk.STOCK_GO_UP,
             '_Select',
             None,
             'Drag the Image',
             0),
            ('FreeHand',
             gtk.STOCK_ITALIC,
             '_Free Hand',
             None,
             'Draw Free Hand',
             1),
            ('Line',
             gtk.STOCK_INDENT,
             '_Line',
             None,
             'Line',
             2),
            ('Rectangle',
             gtk.STOCK_HARDDISK,
             '_Rectangle',
             None,
             'Draw Rectangle',
             3)]
tool_group.add_radio_actions(tool_actions, 0, tool_changed_cb)

ui_spec = '''
    <ui>
    <toolbar name = "Palette">
        <toolitem action = "Select"/>
        <separator/>
        <toolitem action = "FreeHand"/>
        <toolitem action = "Line"/>
        <toolitem action = "Rectangle"/>
    </toolbar>
    </ui>
'''

uimanager = gtk.UIManager()
uimanager.insert_action_group(tool_group, 0)
uimanager.add_ui_from_string(ui_spec)

palette = uimanager.get_widget('/Palette')
palette.set_orientation(gtk.ORIENTATION_VERTICAL)
palette.set_style(gtk.TOOLBAR_BOTH_HORIZ)
palette.set_icon_size(gtk.ICON_SIZE_LARGE_TOOLBAR)

hbox = gtk.HBox()
hbox.pack_start(palette, False)
hbox.pack_start(gtk.VSeparator(), False)
hbox.pack_start(gtkimageview.ImageScrollWin(view))

win = gtk.Window()
win.connect('delete-event', gtk.main_quit)
win.add(hbox)
win.set_default_size(500, 300)
win.show_all()
view.grab_focus()

gtk.main();
