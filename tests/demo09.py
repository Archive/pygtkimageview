'''
Demonstrates the performance difference the overwritable_pixbuf
setting has.
'''
import cairo
import cairotool
import gobject
import gtk
from gtk import gdk
import gtkimageview
import random

class NullShaper(cairotool.ImageCairoShaper):
    def get_damage_area(self):
        return gdk.Rectangle(0, 0, 0, 0)
    def motion(self, x, y):
        pass
    def draw(self, cr):
        pass
    def reset(self):
        pass
    def start(self, x, y):
        pass

gobject.signal_new('toggle_overwrite', gtkimageview.ImageView,
                   gobject.SIGNAL_RUN_LAST | gobject.SIGNAL_ACTION,
                   gobject.TYPE_NONE, ())
gtk.binding_entry_add_signal(gtkimageview.ImageView,
                             gtk.keysyms.t, 0, 'toggle_overwrite')

pixbuf = gtk.gdk.Pixbuf(gtk.gdk.COLORSPACE_RGB, False, 8, 4000, 2000)
view = gtkimageview.ImageView()
view.set_interpolation(gdk.INTERP_NEAREST)
view.set_pixbuf(pixbuf)
tool = cairotool.ImageToolCairo(view, NullShaper())
view.set_tool(tool)

def toggle_cb(*args):
    tool.set_overwritable_pixbuf(not tool.get_overwritable_pixbuf())
view.connect('toggle_overwrite', toggle_cb)    

def do_rectangle_cb(*args):
    surface = tool.get_image_surface()
    width = surface.get_width()
    height = surface.get_height()
    x = random.randint(0, width)
    y = random.randint(0, height)
    width = random.randint(0, width / 5)
    height = random.randint(0, height / 5)
    rect = gdk.Rectangle(x, y, width, height)
    cr = cairo.Context(surface)
    cr.rectangle(*rect)
    cr.set_source_rgba(1, 1, 1, 0.3)
    cr.fill()
    tool.damage_surface(rect)
    return True
gobject.timeout_add(20, do_rectangle_cb)

print 'Press T to toggle the overwritable pixbuf setting.'

win = gtk.Window()
win.connect('delete-event', gtk.main_quit)
win.add(gtkimageview.ImageScrollWin(view))
win.set_default_size(500, 300)
win.show_all()

gtk.main()

