# -*- coding: utf-8 -*-
# Copyright © 2009 Björn Lindqvist <bjourne@gmail.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
'''
This tool will be added to gtkimageview some time. It will take me
long time to implement it all in c..
'''
import cairo
import gobject
import gtk
from gtk import gdk
import gtkimageview
try:
    import psyco
    psyco.full()
except ImportError:
    print "psyco not available. Program won't run fast."

######################################################################
### Extensions to gdk.Rectangle
######################################################################
def gdk_rectangle_get_extents(*points):
    '''
    Returns a bounding box for a number of points.
    '''
    x_points = [x for (x, y) in points]
    y_points = [y for (x, y) in points]
    x1 = min(x_points)
    y1 = min(y_points)
    x2 = max(x_points)
    y2 = max(y_points)
    return gdk.Rectangle(x1, y1, x2 - x1, y2 - y1)

def gdk_rectangle_pad(rect, pad):
    rect.x -= pad
    rect.y -= pad
    rect.width += pad * 2
    rect.height += pad * 2
    return rect

def gdk_pixbuf_copy_to_surface(pixbuf, rect, surface):
    '''
    Copies a rectanguar area in a pixbuf to a cairo surface.
    '''
    context = cairo.Context(surface)
    gdkcontext = gtk.gdk.CairoContext(context)
    gdkcontext.set_source_pixbuf(pixbuf, 0, 0)
    gdkcontext.rectangle(*rect)
    gdkcontext.clip()
    gdkcontext.paint()

def cairo_surface_copy(src_surface, rect, dst_surface):
    '''
    Copies a rectangular area in a cairo surface on another surface.
    '''
    pattern = cairo.SurfacePattern(src_surface)
    cr = cairo.Context(dst_surface)
    cr.set_antialias(cairo.ANTIALIAS_NONE)
    cr.rectangle(*rect)
    cr.clip()
    cr.set_source(pattern)
    cr.set_operator(cairo.OPERATOR_SOURCE)
    cr.paint()

def cairo_surface_to_pixbuf(surface, rect):
    '''
    Returns a new pixbuf created by copying a rectangular selection in
    a cairo surface.

    This function is the reason why the tool is so darn slow.
    '''
    surface_pixels = surface.get_data()
    pixbuf_pixels = [0] * rect.width * rect.height * 3
    surf_width = surface.get_width()
    surf_height = surface.get_height()
    dst_idx = 0
    assert rect.x + rect.width <= surface.get_width()
    assert rect.y + rect.height <= surface.get_height()
    xlines = range(rect.x, rect.x + rect.width)
    ylines = range(rect.y, rect.y + rect.height)
    for y in ylines:
        src_idx = y * surf_width * 4 + rect.x * 4
        for x in xlines:
            pixbuf_pixels[dst_idx + 0] = surface_pixels[src_idx + 2]
            pixbuf_pixels[dst_idx + 1] = surface_pixels[src_idx + 1]
            pixbuf_pixels[dst_idx + 2] = surface_pixels[src_idx + 0]
            dst_idx += 3
            src_idx += 4
    pixbuf_pixels = ''.join(pixbuf_pixels)
    return gtk.gdk.pixbuf_new_from_data(pixbuf_pixels,
                                        gtk.gdk.COLORSPACE_RGB,
                                        False,
                                        8,
                                        rect.width, rect.height,
                                        rect.width * 3)

class ImageCairoShaper:
    '''
    An ImageCairoShaper is a callback class used in conjunction with
    the cairo tool. ImageToolCairo offers a cairo context for cairo
    shapers to paint on and handles the details of ensuring that what
    is drawn shows up in the image view.

    The `start` and `motion` callbacks are called by the cairo tool
    when the mouse is dragged. They allow the shaper to record data
    points which are used in the `draw` callback to draw things. The
    `get_damage_area` should generally return the bounding box of what
    would be drawn.

    Here is a simple example that implements a line drawing tool:

    .. python::

        class LineShaper(cairotool.ImageCairoShaper):
            LINE_WIDTH = 5
            def __init__(self):
                self.base = self.ofs = None
            def start(self, x, y):
                self.base = self.ofs = x, y
            def reset(self):
                self.base = self.ofs = None
            def motion(self, x, y):
                self.ofs = x, y
            def get_damage_area(self):
                x = min(self.base[0], self.ofs[0]) - self.LINE_WIDTH
                y = min(self.base[1], self.ofs[1]) - self.LINE_WIDTH
                width = max(self.base[0], self.ofs[0]) - x + self.LINE_WIDTH * 2
                height = max(self.base[1], self.ofs[1]) - y + self.LINE_WIDTH * 2
                return gdk.Rectangle(x, y, width, height)
            def draw(self, cr):
                if not self.base or not self.ofs:
                    return
                cr.set_line_width(self.LINE_WIDTH)
                cr.set_source_rgba(1, 1, 1, 0.5)
                cr.move_to(*self.base)
                cr.line_to(*self.ofs)
                cr.stroke()
    '''
    def start(self, x, y):
        '''
        Begin recording motion events starting with the point x,
        y. The position is where the user pressed the left mouse
        button to begin drawing.

        :param x: x value of the coordinate where the draw begins.
        :param y: y value of the coordinate where the draw begins.
        '''

    def motion(self, x, y):
        '''
        Record a motion event at the image point x, y.

        :param x: x value of the coordinate where the draw begins.
        :param y: y value of the coordinate where the draw begins.
        '''

    def reset(self):
        '''
        Discard all recorded points. This method is called by the
        cairo tool when the left mouse button is released.
        '''

    def get_damage_area():
        '''
        Return the area, in image coordinates, where the shaper will
        draw in the next draw operation.

        For example, if a line is to be drawn on the coordinates
        (10,10)-(25,20), the damage rectangle would be
        (10,10)-[15,10].

        :return: a ``gdk.Rectangle`` describing where on the image the
            next draw will occur.
        '''

    def draw(self, cr):
        '''
        Draw a shape on the cairo context `cr`. What is drawn will be
        clipped by `ImageToolCairo` to the draw tools last reported
        damage area.

        `ImageToolCairo` may call draw without the draw tool having
        recorded any data.

        :param cr: a cairo context to draw on.
        '''

class ImageToolCairo(gobject.GObject, gtkimageview.IImageTool):
    '''
    ImageToolCairo is an image tool that works as an adapter so that a
    user can draw on the pixbuf managed by ImageView using cairo. 
    
    Pixel Flow
    ==========
    Internally, ImageToolCairo uses two different cairo image surfaces
    to buffer data on. Both have the same pixel format that the pixbuf
    the view is showing has.

    When a pixbuf is loaded, its pixel data is copied to the cairo
    image and draw buffer surfaces. Then when the user presses the
    left mouse button and drags, the cairo shaper is queried using
    `ImageCairoShaper.get_damage_area` for the damage area in image
    space coordinates. The damaged area is then scheduled for redraw
    which involves the following steps:

    1. Copy the damaged area of the image surface to an intermediate
       draw surface.
    2. Call `ImageCairoShaper.draw` with a ``cairo.Context``
       referencing the intermediate surface.
    3. Copy the same damaged area in the intermediate surface to the
       views pixbuf.
    4. Draw the damaged area at the correct location on the widget.
    
    5. When the mouse button is released, `ImageCairoShaper.draw` is
       called with a ``cairo.Context`` referencing the image surface
       so that what is drawn will "stick."
    6. `ImageCairoShaper.reset` is called so that further calls to
       `ImageCairoShaper.draw` wont draw anything and the view is
       redrawn again.
    7. The `ImageView.damage_pixels` is called to alert other widgets
       that the pixbuf data has been modified.
    8. If overwritable_pixbuf is set to False, cairo tool will copy
       the damaged pixbuf area to its cairo surfaces to ensure that
       they stay synchronized.

    Performance Considerations
    ==========================

    It is probably wise to set interpolation to ``gdk.INTERP_NEAREST``
    which is much faster than
    ``gdk.INTERP_BILINEAR``. ``overwritable_pixbuf`` should be set to
    True unless it is absolutely necessary to modify the pixbuf. The
    attribute can be set to False for the duration of the pixbuf
    modification:

    .. python::

        cairo_tool.set_overwritable_pixbuf(False)
        pixbuf = view.get_pixbuf()
        ...modify pixbuf here...
        view.damage_pixels()
        cairo_tool.set_overwritable_pixbuf(True)
    '''
    def __init__(self, view, cairo_shaper):
        '''
        Creates a new cairo tool for the specified view using the
        specified cairo shaper. The default values set are:

        * image_surface : None
        * overwritable_pixbuf : None
        '''
        gobject.GObject.__init__(self)
        self.crosshair = gtk.gdk.Cursor(gtk.gdk.CROSSHAIR)
        self.cache = gtkimageview.PixbufDrawCache()
        self.pixbuf = None
        '''
        Reference to the pixbuf the view uses. Attribute is compared
        with it in pixbuf_changed to determine whether to update the
        image surface or not.
        '''
        self.image_surface = None
        self.draw_surface = None
        '''
        draw_surface is a cairo image surface onto which drawing happens
        before it is blitted to the pixbuf.
        '''
        self.view = view
        self.cairo_shaper = cairo_shaper
        self.dragging = False
        self.redraw_area = None
        self.overwritable_pixbuf = True

    ######################################################################
    ### Properties
    ######################################################################
    def set_cairo_shaper(self, cairo_shaper):
        '''
        Set the cairo shaper to use. 

        :param cairo_shaper: the shaper to use (must not be None).
        '''
        self.cairo_shaper = cairo_shaper

    def get_cairo_shaper(self):
        '''
        Return the cairo shaper used by the tool.
        '''
        return self.cairo_shaper

    def set_overwritable_pixbuf(self, overwritable_pixbuf):
        '''
        Set whether ImageToolCairo can ignore changes to the views
        pixbuf or not.

        If this setting is False, then ImageToolCairo must update its
        internal image surface representation of the image when the
        pixbuf data changes. That is significantly slower than
        ignoring pixbuf changes.

        Note that the cairo tool will still update the image surface
        if the views pixbuf reference is changed.

        The default setting is True.

        :param overwritable_pixbuf: whether pixbuf changes are ignored.
        '''
        self.overwritable_pixbuf = overwritable_pixbuf

    def get_overwritable_pixbuf(self):
        '''
        Return whether the cairo tool ignores changes to the views
        pixbuf data or not.

        :return: whether pixbuf changes are ignored.
        '''
        return self.overwritable_pixbuf

    def get_image_surface(self):
        '''
        Return the cairo image surface used by ImageToolCairo to draw
        on the pixbuf. The surface contains the same pixel data as the
        pixbuf that the associated image view shows. The pixel format
        is ``cairo.FORMAT_ARGB32`` if the pixbuf has an alpha channel,
        otherwise it is ``cairo.FORMAT_RGB24``.

        If the image view does not show a pixbuf then this attribute
        is None.

        Note that the user of this library must take special care to
        ensure that the pixbuf used by ImageView and the surface used
        by ImageToolCairo stay synchronized.

        Describe here how to do that.

        :return: the cairo image surface used by ImageToolCairo or
            None if the view does not show a pixbuf.
        '''
        return self.image_surface

    ######################################################################
    ### Actions
    ######################################################################
    def damage_surface(self, rect = None):
        '''
        This method works analogous to `ImageView.damage_pixels()`. A
        rectangle in the image surface shown is marked as dirty which
        causes the image view to redraw that area.

        :param rect: an image rectangle to mark as dirty or None to
            mark the whole pixbuf as dirty.
        '''
        if not self.pixbuf:
            return
        pixbuf = self.pixbuf
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        if not rect:
            rect = gdk.Rectangle(0, 0, width, height)
        self.set_redraw_area(rect)
        self.update_pixbuf()
        self.surface_changed(rect)
        self.view.damage_pixels(rect)

    ######################################################################
    ### IImageTool interface
    ######################################################################
    def do_motion_notify(self, ev):
        if not self.dragging:
            return
        x = int(ev.x)
        y = int(ev.y)
        rect = self.view.widget_to_image_rect((x, y, 0, 0))
        if not rect:
            return

        old_damage_area = self.cairo_shaper.get_damage_area()
        self.cairo_shaper.motion(rect.x, rect.y)
        new_damage_area = self.cairo_shaper.get_damage_area()

        # Union the areas to get the complete redraw area.
        if not self.redraw_area:
            self.redraw_area = old_damage_area

        self.set_redraw_area(self.redraw_area.union(new_damage_area))
        self.surface_changed(self.redraw_area)

    def do_button_press(self, ev):
        x = int(ev.x)
        y = int(ev.y)
        rect = self.view.widget_to_image_rect((x, y, 0, 0))
        if not rect:
            return
        self.dragging = True
        self.cairo_shaper.start(rect.x, rect.y)
            
    def do_button_release(self, ev):
        if not self.dragging:
            return
        self.dragging = False

        self.set_redraw_area(self.cairo_shaper.get_damage_area())
        
        cr = cairo.Context(self.image_surface)
        cr.rectangle(*self.redraw_area)
        cr.clip()
        self.cairo_shaper.draw(cr)
        self.cairo_shaper.reset()
        self.surface_changed(self.redraw_area)
        self.view.damage_pixels(self.redraw_area)

    def do_pixbuf_changed(self, reset_fit, rect):
        pixbuf = self.view.get_pixbuf()
        if not pixbuf:
            self.image_surface = None
            self.draw_surface = None
            return

        if pixbuf == self.pixbuf and self.overwritable_pixbuf:
            # The pixbuf instance is the same but data in it has
            # changed, we are free to ignore that.
            return

        self.pixbuf = pixbuf
        width = pixbuf.get_width()
        height = pixbuf.get_height()

        format = cairo.FORMAT_RGB24
        if pixbuf.get_has_alpha():
            format = cairo.FORMAT_ARGB32
        if (not self.image_surface or
            self.image_surface.get_width() != width or
            self.image_surface.get_height() != height or
            self.image_surface.get_format() != format):
            self.image_surface = cairo.ImageSurface(format, width, height)
            self.draw_surface = cairo.ImageSurface(format, width, height)

        if not rect:
            rect = gdk.Rectangle(0, 0, width, height)

        # Copy contents to cairo surfaces.
        gdk_pixbuf_copy_to_surface(pixbuf, rect, self.image_surface)
        cairo_surface_copy(self.image_surface, rect, self.draw_surface)
        self.surface_changed(rect)

    def do_paint_image(self, draw_opts, drawable):
        if self.redraw_area:
            self.update_pixbuf()
            self.redraw_area = None
        self.cache.draw(draw_opts, drawable)

    def do_cursor_at_point(self, x, y):
        return self.crosshair

    ######################################################################
    ### Helpers
    ######################################################################
    def surface_changed(self, rect):
        self.cache.invalidate()
        wid_rect = self.view.image_to_widget_rect(rect)
        self.view.queue_draw_area(wid_rect.x - 5, wid_rect.y - 5,
                                  wid_rect.width + 10, wid_rect.height + 10)

    def set_redraw_area(self, rect):
        '''
        Clips the input rectangle against the pixbuf size and sets it
        as the new redraw area.
        '''
        pixbuf = self.pixbuf
        width = pixbuf.get_width()
        height = pixbuf.get_height()
        pixbuf_rect = gdk.Rectangle(0, 0, width, height)
        self.redraw_area = rect.intersect(pixbuf_rect)
    
    def update_pixbuf(self):
        '''
        Update the cairo surfaces and put the result on the pixbuf.
        '''
        # Guard against broken update areas.
        if not self.redraw_area.width or not self.redraw_area.height:
            return
        
        cairo_surface_copy(self.image_surface,
                           self.redraw_area,
                           self.draw_surface)

        cr = cairo.Context(self.draw_surface)
        cr.rectangle(*self.redraw_area)
        cr.clip()
        self.cairo_shaper.draw(cr)
        pb = cairo_surface_to_pixbuf(self.draw_surface, self.redraw_area)
        pb.copy_area(0, 0,
                     self.redraw_area.width, self.redraw_area.height,
                     self.pixbuf,
                     self.redraw_area.x, self.redraw_area.y)
gobject.type_register(ImageToolCairo)
