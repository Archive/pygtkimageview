'''
This example demonstrates how to add your own keybindings to
GtkImageView. It is a Python transliteration of the C program
http://trac.bjourne.webfactional.com/browser/gtkimageview/tests/ex-rotate.c.
'''
import gobject
import gtk
import gtkimageview
import sys

try:
    anim = gtk.gdk.PixbufAnimation(sys.argv[1])
except IndexError:
    print 'Usage: %s image' % sys.argv[0]
    sys.exit(1)
print 'Press E and R to rotate the view counter-clockwise and clockwise.'

def rotate_cb(view, rot):
    pixbuf = view.get_pixbuf()
    if not pixbuf:
        return
    dest = pixbuf.rotate_simple(rot)
    view.set_pixbuf(dest)

# Create a rotation signal with an associated keybinding.
gobject.signal_new('rotate', gtkimageview.AnimView,
                   gobject.SIGNAL_RUN_LAST | gobject.SIGNAL_ACTION,
                   gobject.TYPE_NONE,
                   (gobject.TYPE_INT,))
gtk.binding_entry_add_signal(gtkimageview.AnimView,
                             gtk.keysyms.e, 0, 'rotate',
                             int, gtk.gdk.PIXBUF_ROTATE_COUNTERCLOCKWISE)
gtk.binding_entry_add_signal(gtkimageview.AnimView,
                             gtk.keysyms.r, 0, 'rotate',
                             int, gtk.gdk.PIXBUF_ROTATE_CLOCKWISE)

view = gtkimageview.AnimView()
view.connect('rotate', rotate_cb)
view.set_anim(anim)

scroll_win = gtkimageview.ImageScrollWin(view)

win = gtk.Window()
win.set_default_size(600, 400)
win.add(scroll_win)
win.show_all()

gtk.main()
