# -*- coding: utf-8 -*-
# Copyright © 2009 Björn Lindqvist <bjourne@gmail.com>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
# 02111-1307, USA.
import cairo
import cairotool
import gtkimageview
import gtk
from gtk import gdk

class NullShaper(cairotool.ImageCairoShaper):
    def get_damage_area(self):
        return gdk.Rectangle(0, 0, 0, 0)
    def motion(self, x, y):
        pass
    def draw(self, cr):
        pass
    def reset(self):
        pass
    def start(self, x, y):
        pass
null_shaper = NullShaper()

def test_instantiate():
    view = gtkimageview.ImageView()
    tool = cairotool.ImageToolCairo(view, null_shaper)
    assert tool

def test_button_press():
    view = gtkimageview.ImageView()
    tool = cairotool.ImageToolCairo(view, null_shaper)
    ev = gdk.Event(gdk.BUTTON_PRESS)
    ev.x = 100.0
    ev.y = 100.0
    tool.do_button_press(ev)

def test_button_release():
    view = gtkimageview.ImageView()
    tool = cairotool.ImageToolCairo(view, null_shaper)
    ev = gdk.Event(gdk.BUTTON_RELEASE)
    tool.do_button_release(ev)

def test_button_release_realized():
    view = gtkimageview.ImageView()
    view.size_allocate((0, 0, 50, 50))
    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 50, 50)
    view.set_pixbuf(pixbuf)

    ev = gdk.Event(gdk.BUTTON_RELEASE)
    tool.do_button_release(ev)

def test_widget_to_image_pos():
    view = gtkimageview.ImageView()
    assert not cairotool.widget_to_image_pos(view, 10, 10)

def test_do_button_press_and_release():
    view = gtkimageview.ImageView()
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 5, 5)
    view.set_pixbuf(pixbuf)
    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)
    ev = gdk.Event(gdk.BUTTON_PRESS)
    ev.x = 40.0
    ev.y = 40.0
    tool.do_button_press(ev)
    ev = gdk.Event(gdk.BUTTON_RELEASE)
    ev.x = 40.0
    ev.y = 40.0
    tool.do_button_release(ev)

def test_get_image_surface():
    view = gtkimageview.ImageView()
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 5, 5)
    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)
    assert not tool.get_image_surface()
    view.set_pixbuf(pixbuf)
    assert tool.get_image_surface()
    view.set_pixbuf(None)
    assert not tool.get_image_surface()

def test_get_overwritable_pixbuf():
    view = gtkimageview.ImageView()
    tool = cairotool.ImageToolCairo(view, null_shaper)
    assert tool.get_overwritable_pixbuf()
    tool.set_overwritable_pixbuf(False)
    assert not tool.get_overwritable_pixbuf()
    tool.set_overwritable_pixbuf(True)
    assert tool.get_overwritable_pixbuf()

def test_gdk_pixbuf_copy_to_surface():
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 5, 5)
    pixbuf.fill(0)
    surface = cairo.ImageSurface(cairo.FORMAT_RGB24, 5, 5)
    cairotool.gdk_pixbuf_copy_to_surface(pixbuf,
                                         gdk.Rectangle(0, 0, 5, 5),
                                         surface)
    for r, g, b, a in zip(* [iter(surface.get_data())] * 4):
        assert ord(r) == 0
        assert ord(g) == 0
        assert ord(b) == 0

def test_damage_pixbuf():
    '''
    Pixbuf is overwritable by default so the damage should be ignored.
    '''
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 5, 5)
    pixbuf.fill(0)
    view = gtkimageview.ImageView()
    view.set_pixbuf(pixbuf)
    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)
    pixbuf.fill(0xffffff00)
    view.damage_pixels()

    surface = tool.get_image_surface()
    for r, g, b, a in zip(* [iter(surface.get_data())] * 4):
        assert ord(r) == 0
        assert ord(g) == 0
        assert ord(b) == 0

def test_damage_pixbuf_no_overwrite():
    '''
    If pixbuf is not overwritable, then the damage can't be ignored.
    '''
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 5, 5)
    pixbuf.fill(0)
    view = gtkimageview.ImageView()
    view.set_pixbuf(pixbuf)
    tool = cairotool.ImageToolCairo(view, null_shaper)
    tool.set_overwritable_pixbuf(False)
    view.set_tool(tool)
    pixbuf.fill(0xffffff00)
    view.damage_pixels()

    surface = tool.get_image_surface()
    for r, g, b, a in zip(* [iter(surface.get_data())] * 4):
        assert ord(r) == 255
        assert ord(g) == 255
        assert ord(b) == 255

def test_damage_pixbuf_preserves_surface_identity():
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 5, 5)
    pixbuf.fill(0)
    view = gtkimageview.ImageView()
    view.set_pixbuf(pixbuf)
    tool = cairotool.ImageToolCairo(view, null_shaper)
    tool.set_overwritable_pixbuf(False)
    view.set_tool(tool)
    pixbuf.fill(0xffffff00)
    surface = tool.get_image_surface()
    view.damage_pixels()
    assert tool.get_image_surface() is surface

def test_damage_surface():
    '''
    It should immediately update the surface.
    '''
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 20, 20)
    pixbuf.fill(0)
    view = gtkimageview.ImageView()
    view.set_pixbuf(pixbuf)
    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)

    cr = cairo.Context(tool.get_image_surface())
    cr.set_source_rgba(1, 1, 1, 1)
    cr.paint()
    tool.damage_surface()
    for p in pixbuf.get_pixels():
        assert ord(p) == 255

def test_damage_surface_no_pixbuf():
    view = gtkimageview.ImageView()
    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)
    tool.damage_surface(gdk.Rectangle(0, 0, 1000, 1000))

def test_damage_surface_out_of_bounds():
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 100, 100);

    alloc = gdk.Rectangle(0, 0, 5, 5)

    view = gtkimageview.ImageView()
    view.set_pixbuf(pixbuf)
    view.set_show_frame(False)
    view.window = gdk.Window(None,
                             alloc.width, alloc.height,
                             gdk.WINDOW_TOPLEVEL,
                             0,
                             gdk.INPUT_OUTPUT)
    view.size_allocate(alloc)

    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)
    tool.damage_surface(gdk.Rectangle(0, 0, 1000, 1000))    
    
    ev = gdk.Event(gdk.EXPOSE)
    ev.area = alloc
    view.emit('expose-event', ev)

def test_switch_pixbuf_format():
    view = gtkimageview.ImageView()
    view.set_pixbuf(gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 5, 5))
    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)
    assert tool.get_image_surface().get_format() == cairo.FORMAT_RGB24
    view.set_pixbuf(gdk.Pixbuf(gdk.COLORSPACE_RGB, True, 8, 5, 5))
    assert tool.get_image_surface().get_format() == cairo.FORMAT_ARGB32

def test_dragging_outside():
    class TestShaper(cairotool.ImageCairoShaper):
        def start(self, x, y):
            pass
        def motion(self, x, y):
            pass
        def draw(self, cr):
            pass
        def get_damage_area(self):
            return gdk.Rectangle(0, 0, 100, 100)
    view = gtkimageview.ImageView()
    tool = cairotool.ImageToolCairo(view, TestShaper())
    view.set_tool(tool)
    view.set_pixbuf(gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 50, 50))
    view.size_allocate((0, 0, 50, 50))

    ev = gdk.Event(gdk.BUTTON_PRESS)
    ev.x = 40.0
    ev.y = 40.0
    tool.do_button_press(ev)
    ev = gdk.Event(gdk.MOTION_NOTIFY)
    ev.x = 40.0
    ev.y = 40.0
    tool.do_motion_notify(ev)

def test_image_nav_gets_surface_updates():
    pixbuf = gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 100, 100);
    pixbuf.fill(0)
    alloc = gdk.Rectangle(0, 0, 50, 50)

    view = gtkimageview.ImageView()
    view.set_pixbuf(pixbuf)
    view.set_show_frame(False)
    view.window = gdk.Window(None,
                             alloc.width, alloc.height,
                             gdk.WINDOW_TOPLEVEL,
                             0,
                             gdk.INPUT_OUTPUT)
    view.size_allocate(alloc)

    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)

    nav = gtkimageview.ImageNav(view)
    assert not nav.get_pixbuf()
    nav.show_and_grab(10, 10)
    assert nav.get_pixbuf()
    nav.release()

    # Paint on the surface.
    rect = gdk.Rectangle(0, 0, 100, 100)
    cr = cairo.Context(tool.get_image_surface())
    cr.rectangle(*rect)
    cr.set_source_rgba(1, 1, 1, 1)
    cr.fill()
    tool.damage_surface(rect)
    gtk.main_iteration(True)

    # ImageView should show a black pixbuf
    nav.show_and_grab(10, 10)
    pixbuf = nav.get_pixbuf()
    for x in pixbuf.get_pixels():
        assert ord(x) == 255

def test_pixbuf_changed():
    view = gtkimageview.ImageView()
    tool = cairotool.ImageToolCairo(view, null_shaper)
    view.set_tool(tool)
    view.set_pixbuf(gdk.Pixbuf(gdk.COLORSPACE_RGB, False, 8, 50, 50))
