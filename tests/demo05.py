'''
This example demonstrates how to progressively load images and
animations into an AnimView using a PixbufLoader.
'''
import gobject
import gtk
from gtk import gdk
import gtkimageview
import sys

if __name__ == '__main__':
    try:
        file = open(sys.argv[1])
    except IndexError:
        print 'Usage: %s image' % sys.argv[0]
        sys.exit(1)

    view = gtkimageview.AnimView()
    scroll_win = gtkimageview.ImageScrollWin(view)

    progress = gtk.ProgressBar()

    vbox = gtk.VBox()
    vbox.pack_start(progress, expand = False)
    vbox.pack_end(scroll_win)

    win = gtk.Window()
    win.set_default_size(600, 400)
    win.add(vbox)
    win.show_all()

    loader = gdk.PixbufLoader()

    def area_prepared_cb(loader):
        view.set_anim(loader.get_animation())
    loader.connect('area-prepared', area_prepared_cb)

    def area_updated_cb(loader, x, y, width, height):
        view.damage_pixels((x, y, width, height))
    loader.connect('area-updated', area_updated_cb)

    # Get the number of bytes to load per iteration.
    file.seek(0, 2)
    size = file.tell()
    chunk = size / 1000
    file.seek(0, 0)

    # Simulate progressive loading from a really slow source.
    def load_pixbuf_slowly():
        data = file.read(chunk)
        progress.set_fraction(file.tell() / float(size))
        if not data:
            loader.close()
            return False
        loader.write(data)
        return True
    gobject.timeout_add(50, load_pixbuf_slowly)
    gtk.main()
