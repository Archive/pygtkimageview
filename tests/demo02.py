'''
This is a silly example showing how you could implement scrolling text
using `ImageView`. All the text is prerendered and put on a pixbuf,
which is then scrolled by using `ImageView.set_offset()`. It works,
but wastes much more memory than doing it the correct way.
'''
try:
    import cairo
except ImportError:
    print 'Sorry! cairo is needed to run this demo.'
    raise SystemExit
import gobject
import gtk
from gtk import gdk
import gtkimageview
import random

WIDTH = 200

class CairoToPixbuf:
    def __init__(self, surface):
        self.loader = gdk.PixbufLoader()
        surface.write_to_png(self)
        self.loader.close()
    def write(self, data):
        self.loader.write(data)
    def get_pixbuf(self):
        return self.loader.get_pixbuf()

def make_pixbuf():
    font_size = 12
    words = open(__file__).read().split()

    height = len(words) * font_size

    surface = cairo.ImageSurface(cairo.FORMAT_RGB24, WIDTH, height)

    cr = cairo.Context(surface)
    cr.rectangle(0, 0, WIDTH, height)
    
    cr.select_font_face("Sans")
    cr.set_font_size(font_size)

    line_size = int(font_size * 1.3)
    for idx, word in enumerate(words):
        word_width = cr.text_extents(word)[2]
        x = (WIDTH - word_width) / 2
        y = idx * line_size + line_size
        col = tuple(random.random() for x in range(3))
        
        cr.move_to(x, y)
        cr.set_source_rgb(*col)
        cr.show_text(word)
    cr.show_page()
    return CairoToPixbuf(surface).get_pixbuf()

if __name__ == '__main__':
    pixbuf = make_pixbuf()

    # Setup the view
    view = gtkimageview.ImageView()
    view.set_pixbuf(pixbuf)
    view.set_zoom(1)
    view.set_show_cursor(False)

    # It is this callback that scrolls the view.
    def scroll_view():
        viewport = view.get_viewport()
        view.set_offset(viewport.x, viewport.y + 1)
        # Reset the scrolling if we go out of bounds.
        if viewport.y == view.get_viewport().y:
            view.set_offset(viewport.x, 0)
        return True
    gobject.timeout_add(25, scroll_view)

    win = gtk.Window()
    win.set_default_size(WIDTH, 300)
    win.connect('delete-event', gtk.main_quit)
    win.add(view)
    win.show_all()

    gtk.main()
