#include <pygobject.h>
#include <pygtk/pygtk.h>
#include <gtkimageview/gtkimageview-typebuiltins.h>

void gtkimageview_register_classes (PyObject *d);
void gtkimageview_add_constants (PyObject *m, char *prefix);

extern PyMethodDef gtkimageview_functions[];

DL_EXPORT(void)
init_gtkimageview (void)
{
    PyObject *m, *d;

    init_pygobject ();
    init_pygtk ();

    m = Py_InitModule ("_gtkimageview", gtkimageview_functions);
    d = PyModule_GetDict (m);

    gtkimageview_register_classes (d);

    // Add the constants here, because gtkimageview_add_constants()
    // doesn't like that I use two different prefixes.
    pyg_enum_add(m, "ImageTransp", "GTK_IMAGE_", GTK_TYPE_IMAGE_TRANSP);

    pyg_enum_add(m, "PixbufDrawMethod", "GDK_PIXBUF_",
                 GDK_TYPE_PIXBUF_DRAW_METHOD);

    if (PyErr_Occurred())
        PyErr_Print();
}
